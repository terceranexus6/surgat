# Surgat

## _Who opens all locks..._

Surgat is a collection of security and privacy tools and scripts for Linux created to help users worried about their privacy. It also includes a wiki with tutorials and proposed activities depending on the technical level. 

If you are a regular user who's not into terminal stuff yet, visit [level 1 stuff](https://gitlab.com/terceranexus6/surgat/tree/master/level1). In case you are used to work on the linux terminal, use [level 2 stuff](https://gitlab.com/terceranexus6/surgat/tree/master/level2). Happy hacking.

This repository has [GPL v3](https://opensource.org/licenses/GPL-3.0) licence. 
Enjoy.

![](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)
