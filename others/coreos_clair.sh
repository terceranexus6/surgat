#!/bin/bash

#Download docker compuse
curl -LO https://raw.githubusercontent.com/coreos/clair/05cbf328aa6b00a167124dbdbec229e348d97c04/contrib/compose/docker-compose.yml

#Download clair config
mkdir clair_config && curl -L https://raw.githubusercontent.com/coreos/clair/master/config.yaml.sample -o clair_config/config.yaml

#Update
sed 's/clair-git:latest/clair:v2.0.1/' -i docker-compose.yml && \
	  sed 's/host=localhost/host=postgres password=password/' -i clair_config/config.yaml

#Install docker-compose
sudo apt  install docker-compose

#Start DB
docker-compose up -d postgres

#Download CVE for Clair
curl -LO https://gist.githubusercontent.com/BenHall/34ae4e6129d81f871e353c63b6a869a7/raw/5818fba954b0b00352d07771fabab6b9daba5510/clair.sql
docker run -it \
	    -v $(pwd):/sql/ \
	        --network "${USER}_default" \
		    --link clair_postgres:clair_postgres \
		        postgres:latest \
			        bash -c "PGPASSWORD=password psql -h clair_postgres -U postgres < /sql/clair.sql"

#Deploy Clair
docker-compose up -d clair

#Using Klar. Uncomment the following (and comment the next) if you have Go:
# go get github.com/optiopay/klar
curl -L https://github.com/optiopay/klar/releases/download/v1.5/klar-1.5-linux-amd64 -o /usr/local/bin/klar && chmod +x $_

#Exploring vulns with Klar
CLAIR_ADDR=http://localhost:6060 CLAIR_OUTPUT=Low CLAIR_THRESHOLD=10 \
	  klar quay.io/coreos/clair:v2.0.1

#Installing jq
sudo apt install jq
#pacma apt install jq

#Then jsonize the output:
CLAIR_ADDR=http://localhost:6060 CLAIR_OUTPUT=High CLAIR_THRESHOLD=10 JSON_OUTPUT=true klar postgres:latest | jq



