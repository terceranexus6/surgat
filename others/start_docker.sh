#!/bin/bash
 
if [ -z "$1" ]
then
      echo "You forgot the mode: -h for help -b for bash or --sh for sh"
else
      if [ "$1" = "-h" ]; then
	      echo "Use '-b os' for bash, for example '-b tomcat:6' and '--sh os' for sh for example '--sh tomcat:6'"
      elif [ "$1" = "-b" ]; then
	      if [ -z "$2" ]; then
		      echo "You forgot the OS"
	      else
		      sudo docker run --rm -i -t --entrypoint=/bin/bash $2
	      fi
      elif [ "$1" = "--sh" ]; then
	      if [ -z "$2" ]; then
                      echo "You forgot the OS"
              else
                      sudo docker run --rm -i -t --entrypoint=/bin/sh $2
              fi
      else
	      echo "Something went wrong! Try again"
      fi
fi
