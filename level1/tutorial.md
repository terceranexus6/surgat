# Getting started with privacy


This is for users who are just starting to get themselves used to security and privacy. Don't worry, it won't hurt and you will be safe at the same time you will be able to learn and (just maybe) have fun. 

First of all we will take care of communication. For this, I've prepared a beginners guide to key-pair encryption. Let's start from the beginning...

## GPG cheatsheet

GPG allows you to communicate securely with a person. Basically imagine having a couple of keys. One is public, anyone can take and use it to encrypt files, but the encrypted file can only be opened with a key you keep for yourself, a private key. This way you can use public keys and share your own but even if someone captures the message, would not be able to open it without the private key.

Here's a list of user-friendly tools for gpg wit graphic interface:
    
- [Mailvelope](https://addons.mozilla.org/en-US/firefox/addon/mailvelope/?src=search) for Firefox
- [Enigmail](https://www.enigmail.net/index.php/en/)
    
Also you can check the [eff guide for gpg](https://ssd.eff.org/en/module/how-use-pgp-linux).
And here's a list of commands for using it on the terminal. It's quite fun, give it a try! 

Create a key pair:

```
$ pg --full-generate-key 
```

Encrypt a file:

```
gpg -o file.gpg -e -r example@example.org file
```

Decrypt a file:

```
gpg -o message --decrypt message.gpg
```
Export **public** (please please pretty please don't export your private key, a kitty will cry if you do such a thing, DON'T) key:

```
gpg --armor --export you@example.com > myPUBLICkey.asc
```

Import someone's public key:

```
gpg --import uscertkey.asc
```

You can also find this guide in an open [pad](https://pad.disroot.org/p/r.1f5dfc27af79d618cf7cf41a4f7124c7), I chose an open pad and not a md file in this repository in order to introduce the concept of [Disroot](disroot.org). Disroot offers you some of the services Google would also give to you but... without being an intrusive service ;) For example: email, pads, cloud (using open cloud) and others. 

By the way, if you are wondering if you want another Linux distribution, you can check your needs on the [distrowatch advanced search](https://distrowatch.com/search.php). I've personally tried TAILS for privacy, Elementary, Dabian, Ubuntu, Fedora, Bunsenlabs and Deepin. 

Add-on cheatsheet:

- Use [https everywhere](https://www.eff.org/https-everywhere) for taking care of https (encrypted )
- Use [Privacy badger](https://www.eff.org/privacybadger) for removing coockies that affects your privacy.

For checking some basic OS security save [this script](https://gitlab.com/terceranexus6/surgat/blob/master/level1/scripts/checkstuff.sh) I created and then in the terminal write:
```
$ sudo sh checkstuff.sh
```
This script only includes some basic commands and linux tricks for checking security issues.
![](https://gitlab.com/terceranexus6/surgat/raw/master/images/1.gif)
![](https://gitlab.com/terceranexus6/surgat/raw/master/images/2.gif)

If case you don't remember, `cd` is for entering into a directory, `ls` for listing the files inside it and `cat` for watching inside a file. For more information about how to use all of this in communities I wrote this [manual](http://bit.ly/2F7KHQJ) in spanish.
