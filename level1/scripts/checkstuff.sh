#!/bin/bash

mkdir user_security
cd user_security

echo "checking if empty passwords... "
touch users_without_passwords.txt
awk -F: '($2 == "") {print}' /etc/shadow > users_without_passwords.txt

echo "checking for only one root!"
touch root_users.txt
awk -F: '($3 == "0") {print}' /etc/passwd > root_users.txt

echo "checking passwords default config..."
touch passwords_default.txt
grep "^PASS_MAX_DAYS\|^PASS_MIN_DAYS\|^PASS_WARN_AGE\|^ENCRYPT_METHOD" /etc/login.defs 2>/dev/null > passwords_default.txt

cd ..

