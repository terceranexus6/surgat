# Let's get to it!

I'm assuming you already know hoy to use GPG and openssl, but if not you can quickly check [level 1](https://gitlab.com/terceranexus6/surgat/tree/master/level1). We are discussing here some usefull resources for developing and helping privacy awareness tools. 

First of all I'd like to introduce you to [IPFS](https://ipfs.io/), a decentrilazed protocol, resilient and open sourced. It has amazing docs and I also made (still on it) an [open tool](https://gitlab.com/terceranexus6/tellit/-/wikis/Command-line) based on this technology but adds an encryption layer to it. It's **slow** but it's an interesting tool that needs nodes and developers.

![](https://gitlab.com/terceranexus6/tellit/raw/master/multimedia/example2.gif)
![](https://gitlab.com/terceranexus6/tellit/raw/master/multimedia/example4.gif)

You can also check secure file transfer [wormhole](https://magic-wormhole.readthedocs.io/en/latest/welcome.html#installation), the installation in Debian (9 and above) is `sudo apt install magic-wormhole`. As described in the docs, the basic commands are:

```
wormhole send [args] --text TEXT
wormhole send [args] FILENAME
wormhole send [args] DIRNAME
wormhole receive [args]

``` 
![](https://gitlab.com/terceranexus6/surgat/raw/master/images/3.gif)


For basic hardening analysis you can check [this script](https://gitlab.com/terceranexus6/surgat/blob/master/level1/scripts/checkstuff.sh) and add your own stuff. For that,[Linenum](https://github.com/rebootuser/LinEnum) is a nice option. 

![](https://gitlab.com/terceranexus6/surgat/raw/master/images/1.gif)
![](https://gitlab.com/terceranexus6/surgat/raw/master/images/2.gif)

If you want to try a OS before using it (or check server default vulnerabilities in a controlled environment) you can use **docker**. To make it easier I made a [script](https://gitlab.com/terceranexus6/surgat/blob/master/others/start_docker.sh) for connecting either using bash or sh to a OS. for example:
   
```
$ start_docker -b tomcat:6
```

And then inside you can check stuff, such as `uname -a`, `ldd --version` or others, even use Linenum as previously mentioned. Other commands in this tool are `-h` for help and `--sh` for sh connection.
